import React from 'react';
import Helmet from 'react-helmet';

const TitleComponent = ({title, description}) => {
    let defaultTitle = 'Triangu';
    let defaultDescription = 'Triangu';
    return (
        <Helmet>
            <meta name="description" content={description ? description : defaultDescription} />
            <title>{title ? title : defaultTitle}</title>
        </Helmet>
    );
};

export {TitleComponent};