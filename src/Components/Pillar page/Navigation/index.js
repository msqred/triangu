import React from 'react'
import './index.scss'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {Link, animateScroll as scroll} from "react-scroll";


const Navigation = ({data: {loading, error, pillarNavigations}}) => {

    if (error) return <h1></h1>
    if (!loading) {

        return (
            <div className="pillar-nav">
                {pillarNavigations.map(nav => (
                    <Link
                        key={nav.id}
                        to={nav.navigationUrl}
                        className='pillar-nav--item'
                        activeClass='active'
                        spy={true}
                        smooth={true}
                        offset={-70}
                        duration={500}
                    >
                        <div className="pillar-nav--circle">
                            <span></span>
                        </div>
                        <span>{nav.navigationText}</span>
                    </Link>
                ))}
            </div>
        )
    }
    return <h2></h2>
};


export const navigation = gql`
  {
     pillarNavigations {
        id
        navigationText
        navigationUrl
     }
  }
`

export default graphql(navigation)(Navigation)