import React from 'react'
import HubspotForm from 'react-hubspot-form'
import './index.scss'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'

import formImage from '../../../Assets/svg/form-logo.png'

const TopText = ({data: {loading, error, pillarTopTexts}}) => {


    if (error) return <h1></h1>
    if (!loading) {

        return (
            <div className="pillar-top--text">
                <div className="container">
                    <div className="pillar-top-info"
                         dangerouslySetInnerHTML={{__html: `${pillarTopTexts[0].leftText.html}`}}>

                    </div>
                    {/* <div className="pillar-top-form">
                        <form className="top-form">
                            <p className="form-title">{pillarTopTexts[0].formTitle}</p>
                            <p className="form-description"
                               dangerouslySetInnerHTML={{__html: `${pillarTopTexts[0].description.html}`}}/>
                            <input type="text" placeholder="Your name" required/>
                            <input type="email" placeholder="Your email" required/>
                            <button type="submit">{pillarTopTexts[0].buttonText}</button>
                        </form>
                        <img className="formBg" src={formImage} alt=""/>
                    </div> */}

                    <div className="pillar-top-form">
                        <HubspotForm
                            portalId='4583553'
                            formId='2844bfb2-1078-4339-80c9-f821271a444a'
                            onSubmit={() => console.log('Submit!')}
                            onReady={(form) => console.log('Ready!')}
                            loading={<div></div>}
                        />
                    </div>


                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const topText = gql`
  {
   pillarTopTexts {
    formTitle
    description {
      html
    }
    buttonText
    leftText {
      html
    }
  }
  }
`

export default graphql(topText)(TopText)