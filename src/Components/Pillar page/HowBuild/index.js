import React from 'react'
import './index.scss'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'


const HowBuild = ({data: {loading, error, pillarHowBuilds, pillarHowBuildItemses}}) => {


    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="pillar-how-build">
                <div className="container">
                    <h3 className="title">{pillarHowBuilds[0].title}</h3>
                    <div className="p-h-b--text"
                         dangerouslySetInnerHTML={{__html: `${pillarHowBuilds[0].text.html}`}}>

                    </div>
                    <div className="p-h-b--items">
                        {pillarHowBuildItemses.map(item => (
                            <div className="p-h-b--item">
                                <div className="p-h-b--item--title">{item.title}</div>
                                <div className="text">{item.text}</div>
                            </div>
                        ))}
                    </div>
                    <div className="p-h-b--bottom-text"
                         dangerouslySetInnerHTML={{__html: `${pillarHowBuilds[0].bottomText.html}`}}
                    >

                    </div>
                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const howBuild = gql`
  {
    pillarHowBuilds {
    title
    text {
      html
    }
    bottomText {
      html
    }
  }
  pillarHowBuildItemses {
    title
    text
  }
  }
`

export default graphql(howBuild)(HowBuild)