import React from 'react'
import './index.scss'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'


const HowTest = ({data: {loading, error, pillarHowTests}}) => {


    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="pillar-how-test">
                <div className="container">
                    <h3 className="title">{pillarHowTests[0].title}</h3>
                    <div className="how-test--text"
                         dangerouslySetInnerHTML={{__html: `${pillarHowTests[0].text.html}`}}
                    >

                    </div>
                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const howTest = gql`
  {
    pillarHowTests {
    title
    text {
      html
    }
  }
  }
`

export default graphql(howTest)(HowTest)