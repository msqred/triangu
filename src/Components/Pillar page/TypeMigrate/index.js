import React from 'react'
import './index.scss'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'


const TypeMigrate = ({data: {loading, error, pillarTypeMigrates}}) => {


    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="pillar-type-migrate">
                <div className="container">
                    <h3 className="title">{pillarTypeMigrates[0].title}</h3>
                    <div className="p-t-m--text" dangerouslySetInnerHTML={{ __html: `${pillarTypeMigrates[0].text.html}` }}>

                    </div>
                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const typeMigrate = gql`
  {
   pillarTypeMigrates {
    title
    text {
      html
    }
  }
  }
`

export default graphql(typeMigrate)(TypeMigrate)