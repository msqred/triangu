import React from 'react'
import HubspotForm from 'react-hubspot-form'
import './index.scss'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'


const Conclusion = ({data: {loading, error, pillarConclusions}}) => {


    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="pillar-сonclusion">
                <div className="container">
                    <h3 className="title">{pillarConclusions[0].title}</h3>
                    <div className="сonclusion--text"
                         dangerouslySetInnerHTML={{__html: `${pillarConclusions[0].text.html}`}}
                    >

                    </div>

                    <div className="form-сonclusion">
                        <p className="сonclusion-form--left">
                            {pillarConclusions[0].formLeftText}
                        </p>
                        <div className="сonclusion-form--right">
                            <p className="form--title">{pillarConclusions[0].formTitle}</p>

                            {/* <input type="text" placeholder="Your name"/>
                            <input type="email" placeholder="Your email"/>
                            <button type="submit">Send</button> */}

                        <HubspotForm
                            portalId='4583553'
                            formId='28662495-6a08-48c4-af67-be94ba4dece1'
                            onSubmit={() => console.log('Submit!')}
                            onReady={(form) => console.log('Ready!')}
                            loading={<div></div>}
                        />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const сonclusion = gql`
  {
 pillarConclusions {
  title
  text {
    html
  }
  formLeftText
  formTitle
}
  }
`

export default graphql(сonclusion)(Conclusion)