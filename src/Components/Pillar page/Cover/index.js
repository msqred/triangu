import React from 'react'
import './index.scss'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'


const Cover = ({data: {loading, error, pillarPageCovers}}) => {

    if (error) return <h1></h1>
    if (!loading) {

        return (
            <div className="pillar-cover">
                <div className="pillar-cover--wrap">
                    <h2 className="pillar-cover--h2">{pillarPageCovers[0].headline1}</h2>
                    <h1 className="pillar-cover--h1">{pillarPageCovers[0].headline2}</h1>
                </div>
                <img src={`https://media.graphcms.com/${pillarPageCovers[0].backgroundImage.handle}`}
                     className="pillar-cover--bg"
                     alt=""/>
            </div>
        )
    }
    return <h2></h2>
};


export const cover = gql`
  {
   pillarPageCovers {
    headline1
    headline2
    backgroundImage {
      handle
    }
  } 
  }
`

export default graphql(cover)(Cover)