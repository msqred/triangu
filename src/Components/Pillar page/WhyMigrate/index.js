import React from 'react'
import HubspotForm from 'react-hubspot-form'
import './index.scss'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import formImage from "../../../Assets/svg/form-logo.png";


const WhyMigrate = ({data: {loading, error, pillarWhyMigrateTexts, pillarWhyMigrateItemses, pillarWhyMigrateBottomTexts}}) => {


    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="pillar-top--why-migrate">
                <div className="container">
                    <h2 className="title">{pillarWhyMigrateTexts[0].title}</h2>
                    <div className="pillar-top--why-migrate--info"
                         dangerouslySetInnerHTML={{__html: `${pillarWhyMigrateTexts[0].text.html}`}}></div>
                </div>
                <div className="p-t--w-m--wrap">

                    {pillarWhyMigrateItemses.map(item => (
                        <div className="p-t--w-m--item" key={item.id}>
                            <div className="p-t--w-m--item-text">
                                <h3 className="p-t--w-m--text-title">{item.title}</h3>
                                <div className="p-t--w-m--text-info" dangerouslySetInnerHTML={{ __html: `${item.text.html}` }}>
                                </div>
                            </div>
                            <div className="p-t--w-m--item-image">
                                <img
                                    src={`https://media.graphcms.com/${item.image.handle}`}
                                    alt=""/>
                            </div>
                        </div>
                    ))}
                </div>

                <div className="p-t-w-m--bottom-text">
                    <p>{pillarWhyMigrateBottomTexts[0].text}</p>
                </div>

                <div className="p-t-w-m--form">
                    {/* <form>
                        <p className="p-t-w-m--form--title">Get your copy sent to your inbox straight away</p>
                        <input type="text" placeholder="Your name" required/>
                        <input type="email" placeholder="Your email" required/>
                        <button type="submit">Send</button>
                    </form> */}
                    <p className="p-t-w-m--form--title">Get your copy sent to your inbox straight away</p>
                    <HubspotForm
                            portalId='4583553'
                            formId='bb238c11-746f-4303-9127-174347f0d337'
                            onSubmit={() => console.log('Submit!')}
                            onReady={(form) => console.log('Ready!')}
                            loading={<div></div>}
                        />

                    <img className="formBg" src={formImage} alt=""/>



                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const whyMigrate = gql`
  {
    pillarWhyMigrateTexts {
      title
      text {
        html
      }
    }
    pillarWhyMigrateItemses {
        id
        title
        text {
            html
        }
        image {
            handle
        }
    }
    pillarWhyMigrateBottomTexts {
        text
    }
  }
`

export default graphql(whyMigrate)(WhyMigrate)