import React from 'react'
import './index.scss'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'


const Visualize = ({data: {loading, error, pillarVisualizes}}) => {


    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="pillar-visualize">
                <div className="container">
                    <h3 className="title">{pillarVisualizes[0].title}</h3>
                    <div className="visualize--text"
                         dangerouslySetInnerHTML={{__html: `${pillarVisualizes[0].text.html}`}}
                    >

                    </div>
                    <div className="visualize--image">
                        <img
                            src={`https://media.graphcms.com/${pillarVisualizes[0].image.handle}`}
                            alt=""/>
                    </div>
                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const visualize = gql`
  {
    pillarVisualizes {
    title
    text {
      html
    }
    image {
      handle
    }
  }
  }
`

export default graphql(visualize)(Visualize)