import React from 'react'
import './index.scss'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'


const Mistakes = ({data: {loading, error, pillarMistakeses}}) => {


    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="pillar-mistakes">
                <div className="container">
                    <h3 className="title">{pillarMistakeses[0].title}</h3>
                    <div className="mistakes--text"
                         dangerouslySetInnerHTML={{__html: `${pillarMistakeses[0].text.html}`}}
                    >

                    </div>
                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const mistakes = gql`
  {
    pillarMistakeses {
  title
  text {
    html
  }
}
  }
`

export default graphql(mistakes)(Mistakes)