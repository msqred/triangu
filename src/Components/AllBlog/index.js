import React from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import Header from "./Header";
import './index.scss'
import Footer from "../Landing page/Footer";
import {TitleComponent} from "../TitleComponent";

const AllBlog = ({data: {loading, error, blogses, allBlogs}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (
            <React.Fragment>
                <Header/>
                <TitleComponent title={allBlogs[0].title}
                                description={allBlogs[0].description}/>
                <div className="all-blog">

                    <h1 className="all-blog--h1">{allBlogs[0].h1Title}</h1>

                    <div className="all-blog--wrap">
                        {blogses.map(blog => (
                            <div className="all-blog--item" key={blog.id}>
                                <div className="all-blog--img">
                                    <img
                                        src={`https://media.graphcms.com/${blog.cover.handle}`}
                                        alt=""/>
                                </div>
                                <div className="all-blog--info">
                                    <p className="all-blog--title">
                                        {blog.title}
                                    </p>
                                    <a href={`/blog/${blog.linkAddress}`}
                                       className="all-blog--btn">
                                        <span>Read more</span>
                                    </a>
                                </div>

                            </div>
                        ))}
                    </div>
                </div>
                <Footer/>
            </React.Fragment>
        )
    }
    return (
        <div></div>
    )
}

export const allBlog = gql`
  {
    blogses {
        id
        cover {
          handle
        }
        title
        linkAddress
    }
    allBlogs {
        title
        h1Title
        description
    }
  }
`

export default graphql(allBlog, {
    options: ({match}) => ({
        variables: {
            link: match.params.link
        }
    })
})(AllBlog)