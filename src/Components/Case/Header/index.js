import React from 'react'
import {ReactComponent as Logo} from '../../../Assets/svg/logo.svg';

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {ReactComponent as Close} from "../../../Assets/svg/close.svg";
import {ReactComponent as Menu} from "../../../Assets/svg/menu.svg";



const Header = ({data: {loading, error, headerButtons, pillarPageHeadInfoes, pillarHeaderMenus}}) => {


    function openMenu(e) {
        e.preventDefault();
        let menu = document.querySelector('.pillar-header--nav');
        menu.classList.add('active');
    }

    function closeMenu(e) {
        e.preventDefault();
        let menu = document.querySelector('.pillar-header--nav');
        menu.classList.remove('active');
    }

    if (error) return <h1></h1>
    if (!loading) {

        return (
            <div className="pillar-header">
                <div className="header--wrap">
                    <div className="header--logo">
                        <a href={"/"}><Logo/></a>
                    </div>

                    <div className="burger_menu" onClick={openMenu}>
                        <Menu/>
                    </div>

                    <div className="pillar-header--nav">

                        <div className="pillar-header--nav--wrap">

                            <div className="burger_menu--cls" onClick={closeMenu}>
                                <Close/>
                            </div>

                            {pillarHeaderMenus.map(item => (
                                <div className="nav--link" key={item.id}>
                                    <a
                                        href={item.menuItemUrl}
                                    >
                                        {item.menuItem}
                                    </a>
                                    {(() => {
                                        if (item.submenu.length > 0) {
                                            return (
                                                <span className="submenu">
                                                        {item.submenu.map(function (name, index) {
                                                            return (
                                                                <a
                                                                    href={item.submenuUrl[index]}
                                                                >
                                                                    {name}
                                                                </a>
                                                            );
                                                        })}
                                                    </span>
                                            )
                                        }
                                    })()}


                                </div>
                            ))}

                        </div>
                    </div>

                    <div className="header--btn">
                        <a className="btn"
                           href={headerButtons[0].headerButtonUrl}>{headerButtons[0].headerButtonText}</a>
                    </div>
                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const header = gql`
  {
    headerButtons {
        headerButtonUrl
        headerButtonText
    }
    pillarPageHeadInfoes {
        title
        description
    }
    pillarHeaderMenus {
        id
        menuItem
        menuItemUrl
        submenu
        submenuUrl
    }
  }
`

export default graphql(header)(Header)