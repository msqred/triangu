import React from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import Header from "./Header";
import './index.scss'
import Footer from "../Landing page/Footer";
import {TitleComponent} from "../TitleComponent";

const SingleCase = ({data: {loading, error, cases}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="single-case">
                <Header/>
                <TitleComponent title={cases.headTitle}
                                description={cases.headDescription}/>
                <div className="single-case_f-s">
                    <div className="single-container">
                        <div className="single-case_f-s--left">
                            <h1 className="single-case_f-s-title">{cases.topTitle}</h1>
                            <h2 className="single-case_f-s-description">{cases.topDesciption}</h2>
                        </div>
                        <div className="single-case_f-s--image">
                            <img
                                src={`https://media.graphcms.com/${cases.topImage.handle}`}
                                alt=""/>
                        </div>
                    </div>

                </div>

                <div className="single-case-first-item">
                    <div className="single-container">
                        <div
                            className="single-case-first--text"
                            dangerouslySetInnerHTML={{__html: `${cases.firstItemText.html}`}}
                        >

                        </div>
                        <div className="single-case-first--image">
                            <img
                                src={`https://media.graphcms.com/${cases.firstItemImage.handle}`}
                                alt=""/>
                        </div>
                    </div>

                </div>

                <div className="single-case-second-item">
                    <div className="single-container">
                        <div
                            className="single-case-second--text"
                            dangerouslySetInnerHTML={{__html: `${cases.secondItemText.html}`}}
                        >

                        </div>
                        <div className="single-case-second--image">
                            <img
                                src={`https://media.graphcms.com/${cases.secondItemImage.handle}`}
                                alt=""/>
                        </div>
                    </div>
                </div>

                <div className="single-case-third-item">
                    <div className="single-container">

                        <div className="single-case-third--title">
                            <h3 className="third-title">{cases.thirdItemTitle}</h3>
                        </div>
                        <div
                            className="single-case-third--list"
                            dangerouslySetInnerHTML={{__html: `${cases.thirdItemList.html}`}}
                        ></div>
                    </div>
                </div>

                <div className="single-case-fourth-item">
                    <div className="single-container">
                        <div className="single-case-fourth--title">
                            {cases.fouthItemTitle}
                        </div>
                        <div
                            className="single-case-fourth--items"
                        >
                            {cases.fouthItems.map(item => (
                                <div
                                    className="single-case-fourth--item"
                                    dangerouslySetInnerHTML={{__html: `${item.html}`}}
                                >
                                </div>
                            ))}
                        </div>
                    </div>
                </div>

                <div className="single-case-fiveth-item">
                    <div className="single-container">

                        <div
                            className="single-case-fiveth--text"
                            dangerouslySetInnerHTML={{__html: `${cases.fivthItemText.html}`}}
                        >

                        </div>
                        <div className="single-case-fiveth--image">
                            <img
                                src={`https://media.graphcms.com/${cases.fivthItemImage.handle}`}
                                alt=""/>
                        </div>
                    </div>
                </div>

                <div className="single-case-six-item">
                    <div className="single-container">

                        <div
                            className="single-case-six--text"
                            dangerouslySetInnerHTML={{__html: `${cases.sixItemsTexts.html}`}}
                        >

                        </div>
                        <div className="single-case-six--image">
                            <img
                                src={`https://media.graphcms.com/${cases.sixItemImage.handle}`}
                                alt=""/>
                        </div>
                    </div>
                </div>

                <Footer/>

            </div>
        )
    }
    return <div></div>
}

export const singleCase = gql`
  query singleCase($link: String) {
    cases(where: {linkAddress: $link}) {
    id
    topTitle
    topDesciption
    topImage {
      handle
    }
    firstItemText {
      html
    }
    firstItemImage {
      handle
    }
    secondItemText {
      html
    }
    secondItemImage {
      handle
    }
    thirdItemTitle
    thirdItemList {
      html
    }
    fouthItemTitle
    fouthItems {
      html
    }
    fivthItemText {
      html
    }
    fivthItemImage {
      handle
    }
    sixItemImage {
      handle
    }
    sixItemsTexts {
      html
    }
    headTitle
    headDescription
    linkAddress
  }
}
`

export default graphql(singleCase, {
    options: ({match}) => ({
        variables: {
            link: match.params.link
        }
    })
})(SingleCase)