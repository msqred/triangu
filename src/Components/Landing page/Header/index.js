import React from 'react'
import './index.scss'
import {ReactComponent as Logo} from '../../../Assets/svg/logo.svg';
import {ReactComponent as Close} from '../../../Assets/svg/close.svg';
import {ReactComponent as Menu} from '../../../Assets/svg/menu.svg';
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {Link, animateScroll as scroll} from "react-scroll";


const Header = ({data: {loading, error, headerMenus, headerButtons}}) => {

    function openMenu(e) {
        e.preventDefault();
        let menu = document.querySelector('.header--nav');
        menu.classList.add('active');
    }

    function closeMenu(e) {
        e.preventDefault();
        let menu = document.querySelector('.header--nav');
        menu.classList.remove('active');
    }

    if (error) return <h1></h1>
    if (!loading) {

        return (
            <div className="header">
                <div className="header--wrap">
                    <div className="header--logo">
                        <a href={"/"}><Logo/></a>
                    </div>


                    <div className="burger_menu" onClick={openMenu}>
                        <Menu/>
                    </div>

                    <div className="header--nav">

                        <div className="header--nav--wrap">

                            <div className="burger_menu--cls" onClick={closeMenu}>
                                <Close/>
                            </div>

                            {headerMenus.map(headerMenu => (
                                <Link
                                    key={headerMenu.id}
                                    to={headerMenu.headerMenuUrl}
                                    className='nav--link'
                                    activeClass='active'
                                    spy={true}
                                    smooth={true}
                                    offset={-70}
                                    duration={500}
                                    onClick={closeMenu}
                                >
                                    {headerMenu.headerMenuName}
                                </Link>
                            ))}

                        </div>
                    </div>
                    <div className="header--btn">
                        <a className="btn"
                           href={headerButtons[0].headerButtonUrl}>{headerButtons[0].headerButtonText}</a>
                    </div>
                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const header = gql`
  {
    headerMenus {
        id
        headerMenuName
        headerMenuUrl
    }
    headerButtons {
      headerButtonUrl
      headerButtonText
    }
  }
`

export default graphql(header)(Header)