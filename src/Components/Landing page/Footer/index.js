import React from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'

import './index.scss'

const Footer = ({data: {loading, error, footers, footerPartnerses, footerContactses}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="footer_main">
                <div className="container">

                    <div className="footer_main--left">
                        <div className="footer_main-left--info">
                            <div className="f_m-l--logo">
                                <svg width="154" height="38" viewBox="0 0 154 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0)">
                                        <path d="M62.3853 13.4677H57.8656V26.0584H56.0159V13.4677H51.5137V11.9245H62.3853V13.4677ZM73.2918 20.3592H69.9762V26.0584H68.0567V11.9245H72.7159C74.0212 11.8438 75.3128 12.2288 76.3631 13.0118C76.7989 13.4107 77.1398 13.9032 77.3603 14.4527C77.5808 15.0022 77.6754 15.5945 77.637 16.1857C77.6548 17.0122 77.4103 17.8229 76.9389 18.5004C76.4397 19.1713 75.7534 19.6779 74.967 19.9559L78.2652 25.9531V26.0584H76.2584L73.2918 20.3592ZM69.9762 18.8336H72.8381C73.6342 18.875 74.4174 18.6189 75.0368 18.1147C75.3059 17.8732 75.5188 17.5753 75.6606 17.2419C75.8023 16.9086 75.8694 16.548 75.857 16.1857C75.8763 15.8097 75.8128 15.434 75.671 15.0854C75.5293 14.7368 75.3127 14.424 75.0368 14.1691C74.4013 13.6586 73.5973 13.4081 72.7857 13.4677H69.889V18.8336H69.9762ZM85.8736 26.0584H83.9191V11.9245H85.7863V26.0584H85.8736ZM100.776 22.3758H94.7907L93.4645 26.0584H91.5449L96.9197 11.9245H98.5426L103.935 26.0584H102.015L100.689 22.3758H100.776ZM95.4364 20.8327H100.218L97.7224 14.2217L95.3317 20.8327H95.4364ZM120.286 26.0934H118.331L111.246 15.1687V26.0584H109.397V11.9245H111.246L118.349 22.7967V11.9245H120.199V26.0584L120.286 26.0934ZM137.527 24.3399C137.03 25.0407 136.338 25.5777 135.537 25.883C134.583 26.2367 133.571 26.4091 132.553 26.3915C131.481 26.4065 130.426 26.1277 129.5 25.5849C128.565 25.0071 127.829 24.1568 127.388 23.1474C126.878 22.0502 126.622 20.8512 126.638 19.6403V18.4128C126.522 16.6332 127.049 14.8714 128.121 13.4501C128.642 12.8633 129.287 12.4011 130.008 12.0976C130.73 11.7942 131.51 11.6571 132.292 11.6966C133.552 11.6359 134.791 12.0345 135.782 12.8189C136.714 13.6406 137.3 14.7878 137.422 16.0279H135.59C135.555 15.6236 135.441 15.2302 135.254 14.8703C135.067 14.5104 134.812 14.191 134.502 13.9304C134.193 13.6697 133.835 13.473 133.45 13.3515C133.064 13.23 132.659 13.1861 132.257 13.2222C131.714 13.1904 131.172 13.2938 130.679 13.5232C130.186 13.7527 129.757 14.1011 129.43 14.5374C128.724 15.6812 128.389 17.0168 128.47 18.3602V19.5175C128.393 20.8614 128.775 22.1915 129.552 23.2877C129.889 23.746 130.332 24.1148 130.843 24.3623C131.354 24.6098 131.917 24.7285 132.484 24.7081C133.116 24.7516 133.752 24.6984 134.368 24.5503C134.862 24.4182 135.309 24.1511 135.66 23.7787V20.5521H132.344V19.0265H137.44V24.2872L137.527 24.3399ZM154.087 12.0648V21.692C154.11 22.2929 154.01 22.8922 153.794 23.453C153.578 24.0138 153.25 24.5245 152.831 24.9536C151.896 25.8359 150.658 26.3199 149.376 26.3039H148.887C147.544 26.3739 146.225 25.9218 145.205 25.0413C144.733 24.5937 144.363 24.0481 144.122 23.4424C143.881 22.8368 143.774 22.1855 143.809 21.5341V11.9245H145.659V21.4991C145.644 21.9388 145.718 22.377 145.877 22.787C146.036 23.197 146.277 23.5702 146.584 23.8839C147.26 24.441 148.108 24.7453 148.983 24.7453C149.858 24.7453 150.706 24.441 151.382 23.8839C151.673 23.5662 151.897 23.1928 152.041 22.7861C152.185 22.3793 152.246 21.9475 152.22 21.5166V11.9245L154.087 12.0648Z"
                                              fill="white"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M18.8988 1.64493e-05C22.185 -0.00433324 25.4152 0.85403 28.2697 2.4901C28.0603 4.91003 27.0656 7.40011 23.4883 7.40011H3.92635C5.69015 5.09541 7.95787 3.2293 10.5541 1.94611C13.1504 0.66293 16.0055 -0.00293717 18.8988 1.64493e-05ZM34.6042 8.31197C36 10.3799 36.976 12.7042 37.4762 15.1518C37.9764 17.5994 37.9911 20.1221 37.5194 22.5755C37.0476 25.0288 36.0988 27.3644 34.7271 29.4486C33.3555 31.5327 31.5881 33.3243 29.5261 34.7208L26.769 33.1426C23.3661 31.2312 23.2789 28.7762 24.7098 26.1283C26.0011 23.761 32.7894 11.5035 34.5344 8.36458L34.6042 8.31197ZM19.8062 37.9649H18.8988C13.8896 37.965 9.08522 35.9665 5.5415 32.4087C1.99779 28.851 0.00462868 24.025 3.33444e-06 18.9913C-0.000393104 18.3468 0.0345608 17.7029 0.104706 17.0623C0.931568 16.5136 1.80109 16.0327 2.70482 15.6244C3.74978 15.2035 4.91528 15.1996 5.96299 15.6136C7.0107 16.0275 7.86146 16.828 8.34131 17.8514C9.38833 19.7453 16.8222 32.7568 19.8062 37.9649Z"
                                              fill="white"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.594 14.5547H23.977L18.8291 23.3576L13.594 14.5547Z"
                                              fill="white"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="154" height="38" fill="white"/>
                                        </clipPath>
                                    </defs>
                                </svg>

                            </div>
                            <div className="f_m-l--desc">
                                <p>{footers[0].footerDate}</p>
                            </div>
                        </div>
                        <div className="footer_main-left--contacts">
                            {footerContactses.map(footerContacts => (
                                <p key={footerContacts.id}>
                                    <a
                                        href={footerContacts.footerContactsUrl}>
                                        {footerContacts.footerContactsText}
                                    </a>
                                </p>
                            ))}

                        </div>
                    </div>
                    <div className="footer_main--right">

                        {footerPartnerses.map(footerPartners => (
                            <img
                                key={footerPartners.footerPartnersLogo.id}
                                className="f_m--r--partners"
                                src={`https://media.graphcms.com/${footerPartners.footerPartnersLogo.handle}`}
                                alt=""/>
                        ))}

                    </div>
                </div>

            </div>
        )
    }
    return <h2></h2>
};


export const consultation = gql`
{
  footers {
    footerDate
  }
  footerPartnerses {
    footerPartnersLogo {
      id
      handle
    }
  }
  footerContactses {
    id
    footerContactsText
    footerContactsUrl
  }
}
`

export default graphql(consultation)(Footer)
