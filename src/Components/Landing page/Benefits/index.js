import React from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'

import './index.scss'

const Services = ({data: {loading, error, benefitses, benefitsTitles, benefitsImageRights}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="benefits">
                <div className="container">
                    <h3 className="title">{benefitsTitles[0].benefitsTitle}</h3>

                    <div className="benefits_wrap">

                        <div className="benefits_left">
                            {benefitses.map(benefits => (
                                <div className="benefits_item">
                                    <img
                                        className="benefits_icon"
                                        src={`https://media.graphcms.com/${benefits.icon.handle}`}
                                        alt=""/>
                                    <div className="benefits_text">{benefits.text}</div>
                                </div>
                            ))}
                        </div>

                        <div className="benefits_right">
                            <img src={`https://media.graphcms.com/${benefitsImageRights[0].benefitsImageRight.handle}`} className="benefits_right--img" alt=""/>
                        </div>

                    </div>

                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const services = gql`
  {
    benefitsImageRights {
        benefitsImageRight {
            handle
            }
        }
    benefitsTitles {
        benefitsTitle
        }
    benefitses {
        icon {
            handle
        }
        text
    }
  }
`

export default graphql(services)(Services)
