import React from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'

import './index.scss'

const TechnicalStack = ({data: {loading, error, technicalStackTitles, technicalStackItems}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="stack">
                <div className="container">
                    <h3 className="title">{technicalStackTitles[0].technicalStackTitle}</h3>

                    <div className="stack_wrap">


                        {technicalStackItems.map(technicalStackItem => (

                            <div className="stack_item" key={technicalStackItem.id}>

                                <div className="stack_image">
                                    <img src={`https://media.graphcms.com/${technicalStackItem.technicalStackLogo.handle}`} alt=""/>
                                </div>
                                <p className="stack_name">{technicalStackItem.technicalStackName}</p>

                            </div>

                        ))}


                    </div>

                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const technicalStack = gql`
{
  technicalStackTitles {
    technicalStackTitle
  }
  technicalStackItems {
    id
    technicalStackLogo {
      handle
    }
    technicalStackName
  }
}
`

export default graphql(technicalStack)(TechnicalStack)
