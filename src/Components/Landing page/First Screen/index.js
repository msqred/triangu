import React from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'

import './index.scss'
import {TitleComponent} from "../../TitleComponent";

const FirstScreen = ({data: {loading, error, mainScreens, headInfoes}}) => {
    if (error) return <h1>{mainScreens}</h1>
    if (!loading) {
        return (
            <div>
                <TitleComponent title={headInfoes[0].title} description={headInfoes[0].description} />
                {mainScreens.map(mainScreen => (
                    <div className="first--screen">

                        <div className="first--screen_wrap">

                            <div className="first--screen_left">
                                <h1 className="first--screen_h1">{mainScreen.mainScreenH1}</h1>
                                <h2 className="first--screen_description">{mainScreen.mainScreenDescription}</h2>
                            </div>

                            <div className="first--screen_right">
                                <img
                                    className="first--screen_image"
                                    src={`https://media.graphcms.com/${mainScreen.firstScreenRightImage.handle}`}
                                />
                            </div>

                        </div>

                        <img
                            className="first--screen_bg"
                            src={`https://media.graphcms.com/${mainScreen.firstScreenBackground.handle}`}
                            alt=""
                        />

                    </div>
                ))}
            </div>
        )
    }
    return <h2></h2>
};


export const firstScreen = gql`
  {
  headInfoes {
    title
    description
  }
  mainScreens {
    mainScreenH1
    mainScreenDescription
    firstScreenRightImage {
      handle
    }
    firstScreenBackground {
      handle
    }
  }
}
`

export default graphql(firstScreen)(FirstScreen)
