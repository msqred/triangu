import React from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {ReactComponent as Arrow} from '../../../Assets/svg/Arrow.svg';

import './index.scss'

const Approach = ({data: {loading, error, approachTitles, approaches, approachButtons}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="approach">
                <div className="container">
                    <h3 className="title">{approachTitles[0].approachTitle}</h3>

                    <div className="approach_wrap">

                        {approaches.map(approache => (

                            <div className="approach_item" key={approache.id}>

                                <div className="approach_arrow">
                                    <Arrow/>
                                </div>

                                <div className="approach_item--info">
                                    <div className="approach_item--info--left">
                                        <div className="approach_item--info--img">
                                            <img
                                                src={`https://media.graphcms.com/${approache.approachIcon.handle}`}
                                                alt=""/>
                                        </div>
                                        <div className="approach_item--info--title">{approache.approachTitle}</div>
                                        <div className="approach_item--info--text">{approache.approachText}</div>
                                    </div>

                                    {(() => {
                                        if (approache.useRightText === true) {
                                            return (
                                                <div className="approach_item--info--right">
                                                    <div className="approach_item--info--right-title">
                                                        {approache.approachRightText}
                                                    </div>
                                                    <a
                                                        href={approache.approachRightBtnUrl}
                                                        className="approach_item--info--right-btn">
                                                        <span>{approache.approachRightBtnText}</span>
                                                    </a>
                                                </div>
                                            )
                                        }
                                    })()}

                                </div>

                                <div className="approach_item--image">
                                    <img
                                        src={`https://media.graphcms.com/${approache.approachBigImage.handle}`}
                                        alt=""/>
                                </div>

                            </div>

                        ))}


                    </div>

                    <a
                        className="approach_btn"
                        href={approachButtons[0].approachButtonUrl}
                    >
                        <span>{approachButtons[0].approachButtonText}</span>
                    </a>
                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const approach = gql`
{
  approachTitles {
    approachTitle
  }
  approaches {
    id
    approachIcon {
      handle
    }
    approachTitle
    approachText
    approachBigImage {
      handle
    }
    useRightText
    approachRightText
    approachRightBtnText
    approachRightBtnUrl
  }
  approachButtons {
    approachButtonText
    approachButtonUrl
  }
}
`

export default graphql(approach)(Approach)
