import React from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'

import './index.scss'

const Clients = ({data: {loading, error, clientses, clientsTitles}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="clients">
                <div className="container">
                    <h3 className="title">{clientsTitles[0].clientsTitle}</h3>

                    <div className="clients_wrap">

                        {clientses.map(client => (

                            <div className="clients_item" key={client.id}>

                                <div className="clients_image">
                                    <img
                                        src={`https://media.graphcms.com/${client.clientsLogo.handle}`}
                                         alt={client.clientsName}/>
                                </div>

                                <div className="clients_name">
                                    {client.clientsName}
                                </div>

                            </div>

                        ))}


                    </div>

                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const clients = gql`
{
  clientsTitles {
    clientsTitle
  }
  clientses {
    id
    clientsLogo {
      handle
    }
    clientsName
  }
}
`

export default graphql(clients)(Clients)
