import React from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'

import './index.scss'

const InfoCenter = ({data: {loading, error, infoCenterTitles, infoCenterItems}}) => {


    if (error) return <p></p>
    if (!loading) {

        return (
            <div className="resources">
                <div className="container">
                    <h3 className="title">{infoCenterTitles[0].infoCenterTitle}</h3>

                    <div className="resources_wrap">

                        {/*<div className="slider_arrow">
                            <div className="arrow arrow_back">
                                <svg width="36" height="30" viewBox="0 0 36 30" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M0.54171 16.05L12.5417 28.7167C13.2152 29.3925 14.6598 29.6251 15.4475 28.877C16.2353 28.129 16.2304 26.7093 15.4375 25.9667L6.64592 16.675L34 16.675C35.1045 16.675 36 15.7796 36 14.675C36 13.5704 35.1045 12.675 34 12.675L6.64592 12.675L15.4375 3.38336C16.2305 2.64076 16.2829 1.16761 15.4475 0.47308C14.6756 -0.168787 13.2423 -0.196775 12.5417 0.633358L0.541702 13.3C0.0317955 13.8114 -0.366253 15.157 0.54171 16.05Z"
                                        fill="#36A5ED"/>
                                </svg>
                            </div>
                            <div className="arrow arrow_next">
                                <svg width="36" height="30" viewBox="0 0 36 30" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M35.4583 13.2908L23.4583 0.624106C22.7848 -0.0516815 21.3402 -0.284294 20.5525 0.463787C19.7647 1.21186 19.7696 2.6315 20.5625 3.37411L29.3541 12.6658H2C0.895467 12.6658 0 13.5612 0 14.6658C0 15.7704 0.895467 16.6658 2 16.6658H29.3541L20.5625 25.9575C19.7695 26.7001 19.7171 28.1732 20.5525 28.8677C21.3244 29.5096 22.7577 29.5376 23.4583 28.7075L35.4583 16.0408C35.9682 15.5294 36.3663 14.1838 35.4583 13.2908Z"
                                        fill="#36A5ED"/>
                                </svg>
                            </div>
                        </div>*/}

                        <div className="resources_items">

                            {infoCenterItems.map(infoCenterItem => (

                                <div className="resources_item" key={infoCenterItem.id}>


                                    {(() => {
                                        if (infoCenterItem.type === 'PillarPage') {
                                            return (
                                                <div className="resources_type">
                                                    Pillar Page
                                                </div>
                                            )
                                        } else {
                                            return (
                                                <div className="resources_type">
                                                    {infoCenterItem.type}
                                                </div>
                                            )
                                        }
                                    })()}


                                    <div className="resources_text">
                                        {infoCenterItem.description}
                                    </div>
                                    <a
                                        href={infoCenterItem.buttonUrl}
                                        className="resources_btn">
                                        <span>{infoCenterItem.buttonText}</span>
                                    </a>

                                </div>

                            ))}

                        </div>


                    </div>

                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const infoCenter = gql`
{
  infoCenterTitles {
    infoCenterTitle
  }
  infoCenterItems {
    id
    description
    buttonUrl
    buttonText
    type
  }
}
`

export default graphql(infoCenter)(InfoCenter)
