import React from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'

import './index.scss'

const Services = ({data: {loading, error, serviceButtons, serviceTitles, serviceses}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="services" id="services">
                <div className="container">
                    <h3 className="title">{serviceTitles[0].serviceTitle}</h3>

                    <div className="services_wrap">

                        {serviceses.map(service => (
                            <div className="services_item">
                                <div className="services_icon">
                                    <img
                                        src={`https://media.graphcms.com/${service.servicesItemIcon.handle}`}
                                    />
                                </div>
                                <div className="services_title">{service.servicesItemTitle}</div>
                                <div className="services_text">{service.servicesItemText}</div>
                            </div>
                        ))}

                    </div>

                    <a
                        className="service_btn"
                        href={serviceButtons[0].serviceButtonUrl}
                    >
                        <span>{serviceButtons[0].serviceButtonText}</span>
                    </a>
                </div>
            </div>
        )
    }
    return <h2></h2>
};


export const services = gql`
  {
    serviceButtons {
        serviceButtonText
        serviceButtonUrl
    }
    serviceTitles {
        serviceTitle
    }
    serviceses {
        servicesItemIcon {
          handle
        }
        servicesItemTitle
        servicesItemText
    }
  }
`

export default graphql(services)(Services)
