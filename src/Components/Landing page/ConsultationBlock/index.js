import React from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'

import './index.scss'

const Consultation = ({data: {loading, error, consultations}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="consultations">
                <div className="container">

                    <div className="consultations_wrap">
                        <p className="consultations_text">{consultations[0].consultationText}</p>
                        <a
                            href={consultations[0].consultationBtnUrl}
                            className="consultations_btn"
                        >
                            <span>{consultations[0].consultationBtnText}</span>
                        </a>
                    </div>
                </div>
                <img
                    src={`https://media.graphcms.com/${consultations[0].consultationBackground.handle}`}
                    alt=""
                    className="consultations_bg"
                />
            </div>
        )
    }
    return <h2></h2>
};


export const consultation = gql`
{
  consultations {
    consultationText
    consultationBtnUrl
    consultationBtnText
    consultationBackground {
      handle
    }
  }
}
`

export default graphql(consultation)(Consultation)
