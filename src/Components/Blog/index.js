import React from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import Header from "./Header";
import './index.scss'
import Footer from "../Landing page/Footer";
import {TitleComponent} from "../TitleComponent";

const SingleBlog = ({data: {loading, error, blogs}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="single-blog">
                <Header/>
                <TitleComponent title={blogs.headTitle}
                                description={blogs.headDescription}/>
                <div className="single-blog--cover">
                    <div className="single-blog--cover--wrap">
                        <h1>{blogs.title}</h1>
                    </div>
                    <img
                        alt={blogs.title}
                        src={`https://media.graphcms.com/${blogs.cover.handle}`}
                    />
                </div>

                <div className="single-blog--text">
                    <div className="single-blog--text--p"
                         dangerouslySetInnerHTML={{__html: `${blogs.text[0].html}`}}
                    />
                    <img
                        alt=""
                        src={`https://media.graphcms.com/${blogs.image.handle}`}
                    />
                    <div className="single-blog--text--p"
                         dangerouslySetInnerHTML={{__html: `${blogs.text[1].html}`}}
                    />
                </div>

                <Footer/>

            </div>
        )
    }
    return (
        <div></div>
    )
}

export const singleBlog = gql`
  query singleBlog($link: String) {
    blogs(where: {linkAddress: $link}) {
      id
      title
      cover {
        id
        handle
      }
      text {
        html
      }
      image {
        handle
      }
      headTitle
      headDescription
      linkAddress
    }
  }
`

export default graphql(singleBlog, {
    options: ({match}) => ({
        variables: {
            link: match.params.link
        }
    })
})(SingleBlog)