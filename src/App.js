import React from 'react'
import {BrowserRouter as Router, Route} from 'react-router-dom'

import Landing from './Pages/Landing page'
import Pillar from './Pages/Pillar page'
import Blog from './Components/Blog'
import Case from './Components/Case'
import AllBlog from './Components/AllBlog'


const App = () => (

    <Router>
        <div>

            <main>
                <Route exact path='/cloud-migration-services' component={Landing}/>
                <Route path='/cloud-migration-strategy' component={Pillar}/>
                <Route exact path='/blog' component={AllBlog}/>
                <Route path='/blog/:link' component={Blog}/>
                <Route path='/cases/:link' component={Case}/>
            </main>
        </div>
    </Router>
);

export default App;
