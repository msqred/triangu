import React from 'react';


import './index.scss'
import Header from "../../Components/Pillar page/Header";
import Cover from "../../Components/Pillar page/Cover";
import TopText from "../../Components/Pillar page/TopText";
import Navigation from "../../Components/Pillar page/Navigation";
import WhyMigrate from "../../Components/Pillar page/WhyMigrate";
import TypeMigrate from "../../Components/Pillar page/TypeMigrate";
import HowBuild from "../../Components/Pillar page/HowBuild";
import Visualize from "../../Components/Pillar page/Visualize";
import HowTest from "../../Components/Pillar page/HowTest";
import Mistakes from "../../Components/Pillar page/Mistakes";
import Conclusion from "../../Components/Pillar page/Conclusion";
import Footer from "../../Components/Landing page/Footer";

const Pillar = () => {

    return (
        <React.Fragment>
            <Header/>
            <Cover/>
            <TopText/>
            <Navigation/>
            <WhyMigrate/>
            <TypeMigrate/>
            <HowBuild/>
            <Visualize/>
            <HowTest/>
            <Mistakes/>
            <Conclusion/>
            <Footer/>
        </React.Fragment>
    )
}

export default Pillar;