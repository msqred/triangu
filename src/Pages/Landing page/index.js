import React from 'react';
import Header from "../../Components/Landing page/Header";
import FirstScreen from "../../Components/Landing page/First Screen";
import Services from "../../Components/Landing page/Services";
import Benefits from "../../Components/Landing page/Benefits";
import Approach from "../../Components/Landing page/Approach";
import Clients from "../../Components/Landing page/Clients";
import InfoCenter from "../../Components/Landing page/InfoCenter";
import TechnicalStack from "../../Components/Landing page/TechnicalStack";
import Consultation from "../../Components/Landing page/ConsultationBlock";
import Footer from "../../Components/Landing page/Footer";

import './index.scss'


const Landing = () => {

    return (
        <React.Fragment>
            <Header/>
            <FirstScreen/>
            <Services/>
            <Benefits/>
            <Approach/>
            <Clients/>
            <InfoCenter/>
            <TechnicalStack/>
            <Consultation/>
            <Footer/>
        </React.Fragment>
    )
}

export default Landing;