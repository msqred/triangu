# Stage 1 - the build process
FROM node:10 as build-deps
# Create app directory
WORKDIR /usr/src/app
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./
# RUN npm install
# If you are building your code for production
RUN npm ci --only=production

# Bundle app source
COPY . .
RUN npm run build

# Stage 2 - the environment
FROM nginx:1.17-alpine
COPY --from=build-deps /usr/src/app/build /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]